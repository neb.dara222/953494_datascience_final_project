import Dashboard from 'views/Dashboard.js';
import UserProfile from 'views/UserProfile.js';
import TableList from 'views/TableList.js';
import AgeChart from 'views/AgeChart';
import GenderChart from 'views/GenderChart';
const dashboardRoutes = [
    {
        path: '/general',
        name: 'General',
        icon: 'pe-7s-display2',
        component: Dashboard,
        layout: '/admin',
    },
    {
        path: '/age',
        name: 'Age',
        icon: 'pe-7s-graph2',
        component: AgeChart,
        layout: '/admin',
    },
    {
        path: '/gender',
        name: 'Gender',
        icon: 'pe-7s-graph',
        component: GenderChart,
        layout: '/admin',
    },


    {
        path: '/data',
        name: 'Data',
        icon: 'pe-7s-note2',
        component: TableList,
        layout: '/admin',
    },
    {
        path: '/prediction',
        name: 'Prediction',
        icon: 'pe-7s-user',
        component: UserProfile,
        layout: '/admin',
    },
];

export default dashboardRoutes;
