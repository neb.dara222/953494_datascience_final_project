import React, { Component } from 'react';
import ChartistGraph from 'react-chartist';
import { Grid, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import _ from 'lodash';
import { Card } from 'components/Card/Card.js';
import { StatsCard } from 'components/StatsCard/StatsCard.js';
import { Tasks } from 'components/Tasks/Tasks.js';
import {
    dataPie,
    legendPie,
    dataSales,
    optionsSales,
    responsiveSales,
    legendSales,
    dataBar,
    optionsBar,
    responsiveBar,
    legendBar,
} from 'variables/Variables.js';
import { ACCUMMULATIVE_CASES } from 'variables/ConstantAPI';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmed_date: [],
            confirmed_case: [],
            death_case: [],
            recovered_case: [],
            newConfirmed_case: [],
            newDeath_case: [],
            newRecovered_case: [],
        };
    }
    createLegend(json) {
        var legend = [];
        for (var i = 0; i < json['names'].length; i++) {
            var type = 'fa fa-circle text-' + json['types'][i];
            legend.push(<i className={type} key={i} />);
            legend.push(' ');
            legend.push(json['names'][i]);
        }
        return legend;
    }
    componentDidMount() {
        axios
            .get(ACCUMMULATIVE_CASES)

            .then((response) => {
                const accumulativeCase = response.data.accumulate_case;
                this.setState({
                    confirmed_case: accumulativeCase.Confirmed,
                    death_case: accumulativeCase.Deaths,
                    recovered_case: accumulativeCase.Recovered,
                    confirmed_date: accumulativeCase.Date,
                    newConfirmed_case: accumulativeCase.newConfirmed,
                    newDeath_case: accumulativeCase.newDeath,
                    newRecovered_case: accumulativeCase.newRecovered,
                });
                console.log('Response', response.data.accumulate_case);
            })
            .catch((err) => {
                console.log(err);
            });
    }
    render() {
        const {
            confirmed_case,
            recovered_case,
            death_case,
            confirmed_date,
            newConfirmed_case,
        } = this.state;
        const cases_data = [];
        cases_data.push(confirmed_case, death_case, recovered_case);
        console.log(cases_data);
        const plot_covid_19_data = _.assign({
            labels: confirmed_date,
            series: cases_data,
        });
        const total_cases =
            _.last(confirmed_case) +
            _.last(death_case) +
            _.last(recovered_case);
        const plot_pie = _.assign({
            labels: [
                `${_.floor((_.last(confirmed_case) * 100) / total_cases)}%`,
                `${_.floor((_.last(death_case) * 100) / total_cases)}%`,
                `${_.floor((_.last(recovered_case) * 100) / total_cases)}%`,
            ],
            series: [
                (_.last(confirmed_case) * 100) / total_cases,
                (_.last(death_case) * 100) / total_cases,
                (_.last(recovered_case) * 100) / total_cases,
            ],
        });

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col lg={3} sm={3}>
                            <StatsCard
                                bigIcon={
                                    <i className="fa fa-heartbeat text-warning" />
                                }
                                statsText="Confirmed Case"
                                statsValue={_.last(confirmed_case)}
                                statsIcon={<i className="fa fa-refresh" />}
                                statsIconText="Updated now"
                            />
                        </Col>
                        <Col lg={3} sm={3}>
                            <StatsCard
                                bigIcon={
                                    <i className="fa fa-frown-o text-danger" />
                                }
                                statsText="Deaths"
                                statsValue={_.last(death_case)}
                                statsIcon={<i className="fa fa-refresh" />}
                                statsIconText="Updated now"
                            />
                        </Col>
                        <Col lg={3} sm={3}>
                            <StatsCard
                                bigIcon={
                                    <i className="fa fa-smile-o text-success" />
                                }
                                statsText="Recovered"
                                statsValue={_.last(recovered_case)}
                                statsIcon={<i className="fa fa-refresh" />}
                                statsIconText="Updated now"
                            />
                        </Col>
                        <Col lg={3} sm={3}>
                            <StatsCard
                                bigIcon={
                                    <i className="fa fa-user-plus text-info" />
                                }
                                statsText="New recovered"
                                statsValue={_.last(newConfirmed_case)}
                                statsIcon={<i className="fa fa-refresh" />}
                                statsIconText="Updated now"
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={8}>
                            <Card
                                statsIcon="fa fa-history"
                                id="chartHours"
                                title="Confirmed vs Deaths vs Recovered"
                                category={`${_.size(
                                    confirmed_date
                                )} Days Performance`}
                                stats={`Data ${_.head(
                                    confirmed_date
                                )} - ${_.last(confirmed_date)}`}
                                content={
                                    <div className="ct-chart">
                                        <ChartistGraph
                                            data={plot_covid_19_data}
                                            type="Line"
                                            options={optionsSales}
                                            responsiveOptions={responsiveSales}
                                        />
                                    </div>
                                }
                                legend={
                                    <div className="legend">
                                        {this.createLegend(legendSales)}
                                    </div>
                                }
                            />
                        </Col>
                        <Col md={4}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                title="Covid-19 Statistics"
                                category={`${_.size(
                                    confirmed_date
                                )} Days Performance`}
                                stats={`Data ${_.head(
                                    confirmed_date
                                )} - ${_.last(confirmed_date)}`}
                                content={
                                    <div
                                        id="chartPreferences"
                                        className="ct-chart ct-perfect-fourth"
                                    >
                                        <ChartistGraph
                                            data={plot_pie}
                                            type="Pie"
                                        />
                                    </div>
                                }
                                legend={
                                    <div className="legend">
                                        {this.createLegend(legendPie)}
                                    </div>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Dashboard;
