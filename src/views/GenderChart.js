import React, { Component } from 'react';
import axios from 'axios';
import {Bar, Pie} from 'react-chartjs-2';
import { back } from 'variables/BackgroundBarChart';
import { Grid } from 'react-bootstrap';

class GenderChart extends Component {
  constructor(props) {  

                super(props);  

                this.state = { DataOfConfirmed: {},DataOfDeath: {},DataOfRecovered: {}};  
        } 

  componentDidMount() {
    axios
    .get(`http://127.0.0.1:5000/covid19_case_by_gender`)
      .then((response) =>{
        const accumulativeCase = response.data.accumulate_gendercase;
        let gender=[];
        let confirmed=[];
        let death=[];
        let recovered=[];
        gender.push(accumulativeCase.gender);
        confirmed.push(accumulativeCase.confirmed);
        death.push(accumulativeCase.death);
        recovered.push(accumulativeCase.recovered)
        console.log(confirmed);
        console.log(gender)
        console.log(death);
        console.log(recovered);
        this.setState({ 
        DataOfConfirmed:{
            labels:gender[0],
            datasets:[
              {
                label:['Gender'],
                data:confirmed[0],
                backgroundColor:back
              }
            ]
          },
          DataOfDeath:{
            labels:gender[0],
            datasets:[
              {
                label:['Gender'],
                data:death[0],
                backgroundColor:back
              }
            ]
          },
          DataOfRecovered:{
            labels:gender[0],
            datasets:[
              {
                label:['Gender'],
                data:recovered[0],
                backgroundColor:back
              }
            ]
          }
          });
        console.log('Response', response.data.accumulate_case);
    })
    .catch((err) => {
        console.log(err);
    });
  
  }

  render() {
    return (
        <div className="content">
            <Grid fluid>
          <h2>The Gender of confirmed</h2>
        
          <div>  

                                <Pie data={this.state.DataOfConfirmed}  

                                        options={{ maintainAspectRatio: false }} ></Pie>  
                                        </div>     
                                        </Grid>
                                        <Grid fluid>
          <h2>The Gender of death</h2>
        
          <div>  

                                <Pie data={this.state.DataOfDeath}  

                                        options={{ maintainAspectRatio: false }} ></Pie>  
                                        </div>     
                                        </Grid>
                                        <Grid fluid>
          <h2>The Gender of recovered</h2>
        
          <div>  

                                <Pie data={this.state.DataOfRecovered}  

                                        options={{ maintainAspectRatio: false }} ></Pie>  
                                        </div>     
                                        </Grid>
      </div>
  
    )
  }
}

export default GenderChart;