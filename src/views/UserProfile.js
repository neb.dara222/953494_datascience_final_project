import React, { Component } from 'react';
import { Grid, Row, Col, Form } from 'react-bootstrap';
import _ from 'lodash';
import axios from 'axios';
import ChartistGraph from 'react-chartist';
import {
    dataPie,
    legendPie,
    dataSales,
    optionsSales1,
    responsiveSales,
    legendSales,
    dataBar,
    optionsBar,
    responsiveBar,
    legendBar,
} from 'variables/Variables.js';

import { Card } from 'components/Card/Card.js';
import { PREDICTION_CONFIRMED_CASE } from 'variables/ConstantAPI';

class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            adjusted_dates: [],
            future_forcast_date: [],
            world_cases: [],
            prediction: [],
            selection: 'prediction_svm_model',
        };
    }
    createLegend(json) {
        var legend = [];
        for (var i = 0; i < json['names'].length; i++) {
            var type = 'fa fa-circle text-' + json['types'][i];
            legend.push(<i className={type} key={i} />);
            legend.push(' ');
            legend.push(json['names'][i]);
        }
        return legend;
    }

    _handleChange = event => {
        this.setState({
            selection: event.target.value,
        });
    };

    _renderAPI = __keyword_search__ => {
        return axios
            .get(`http://127.0.0.1:5000/${__keyword_search__}`)

            .then(response => {
                console.log('Response', response.data);
                const prediction_data = response.data.prediction_models.model;
                this.setState({
                    adjusted_dates: prediction_data.adjusted_dates,
                    future_forcast_date: prediction_data.future_forcast,
                    world_cases: prediction_data.world_cases,
                    prediction: prediction_data.prediction,
                });
            })
            .catch(error => {
                console.warn(error);
            });
    };

    render() {
        const {
            adjusted_dates,
            future_forcast_date,
            world_cases,
            prediction,
            selection,
        } = this.state;
        const prediction_arrange = _.assign({
            labels: _.flatten(future_forcast_date),
            series: [_.flatten(world_cases), _.flatten(prediction)],
        });
        this._renderAPI(selection);

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <form>
                                <select
                                    onChange={this._handleChange}
                                    style={{ width: 200 }}
                                    placeholder="Select a prediction model"
                                    value={selection}
                                    name="selection"
                                >
                                    <option value="prediction_linear_regression_model">
                                        Linear Regression
                                    </option>
                                    <option value="prediction_svm_model">
                                        Support Vector Machince
                                    </option>
                                    <option value="prediction_bayesian_model">
                                        Bayesian Prediction
                                    </option>
                                </select>
                            </form>
                            <Card
                                statsIcon="fa fa-history"
                                id="chartHours"
                                title="7 Days Prediction"
                                category={'Data 01/22/2020 - 03/24/2020'}
                                stats={
                                    'Prediction period | 01/22/2020 - 03/31/2020'
                                }
                                content={
                                    <div>
                                        <ChartistGraph
                                            data={prediction_arrange}
                                            type="Line"
                                            options={optionsSales1}
                                            responsiveOptions={responsiveSales}
                                        />
                                    </div>
                                }
                                legend={
                                    <div className="legend">
                                        {this.createLegend({
                                            names: [
                                                'confirmed_case',
                                                selection,
                                            ],
                                            types: ['info', 'danger'],
                                        })}
                                    </div>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default UserProfile;
