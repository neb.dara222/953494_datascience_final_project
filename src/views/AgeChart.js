import React, { Component } from 'react';
import axios from 'axios';
import { Bar } from 'react-chartjs-2';
import { back } from 'variables/BackgroundBarChart';
import { Grid } from 'react-bootstrap';

class AgeChart extends Component {
    constructor(props) {
        super(props);

        this.state = { DataOfConfirmed: {}, DataOfDeath:{} ,DataOfRecovered:{}};
    }

    componentDidMount() {
        axios
            .get(`http://127.0.0.1:5000/covid19_case_by_age`)
            .then((response) => {
                const accumulativeCase = response.data.accumulate_agecase;
                let age = [];
                let confirmed = [];
                let death =[];
                let recovered=[];
                age.push(accumulativeCase.age);
                confirmed.push(accumulativeCase.confirmed);
                death.push(accumulativeCase.death);
                recovered.push(accumulativeCase.recovered);
                console.log(confirmed);
                console.log(age);
                console.log(death);
                console.log(recovered);
                this.setState({
                    DataOfConfirmed: {
                        labels: age[0],
                        datasets: [
                            {
                                label: ['Age'],
                                data: confirmed[0],
                                backgroundColor: back,
                            },
                        ],
                    },
                    DataOfDeath: {
                        labels: age[0],
                        datasets: [
                            {
                                label: ['Age'],
                                data: death[0],
                                backgroundColor: back,
                            },
                        ],
                    },
                    DataOfRecovered: {
                        labels: age[0],
                        datasets: [
                            {
                                label: ['Age'],
                                data: recovered[0],
                                backgroundColor: back,
                            },
                        ],
                    },
                });
                console.log('Response', response.data.accumulate_case);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    render() {
        return (
            <div className="content">
                <Grid fluid>
                    <h2>The age of confirmed</h2>

                    <div>
                        
                        <Bar
                            data={this.state.DataOfConfirmed}
                            options={{ maintainAspectRatio: false }}
                        ></Bar>
                    </div>
                </Grid>
                <Grid fluid>
                    <h2>The age of death</h2>

                    <div>
                        <Bar
                            data={this.state.DataOfDeath}
                            options={{ maintainAspectRatio: false }}
                        ></Bar>
                    </div>
                </Grid>
                <Grid fluid>
                    <h2>The age of recoverd</h2>

                    <div>
                        <Bar
                            data={this.state.DataOfRecovered}
                            options={{ maintainAspectRatio: false }}
                        ></Bar>
                    </div>
                </Grid>
            </div>
        );
    }
}

export default AgeChart;