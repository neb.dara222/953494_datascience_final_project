import React, { Component } from 'react';
import { Grid, Row, Col, Table } from 'react-bootstrap';
import _ from 'lodash';
import Card from 'components/Card/Card.js';
import { thArray, tdArray } from 'variables/Variables.js';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Axios from 'axios';
import { ACCUMMULATIVE_CASES } from 'variables/ConstantAPI';

class TableList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            confirmed_date: [],
            confirmed_case: [],
            death_case: [],
            recovered_case: [],
            newConfirmed_case: [],
            newDeath_case: [],
            newRecovered_case: [],
        };
    }
    

    componentDidMount() {
        Axios
            .get(ACCUMMULATIVE_CASES)
            .then((response) => {
                
                const accumulativeCase = response.data.accumulate_case;
                this.setState({
                    confirmed_case: accumulativeCase.Confirmed,
                    death_case: accumulativeCase.Deaths,
                    recovered_case: accumulativeCase.Recovered,
                    confirmed_date: accumulativeCase.Date,
                    newConfirmed_case: accumulativeCase.newConfirmed,
                    newDeath_case: accumulativeCase.newDeath,
                    newRecovered_case: accumulativeCase.newRecovered,
                });
                console.log('Response', response.data.accumulate_case);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    render() {
        const { confirmed_case, confirmed_date,  death_case,
            recovered_case,
            newConfirmed_case,
            newDeath_case,
            newRecovered_case } = this.state;
            const dataTable  = {};

        for (let i = 0; i < _.size(confirmed_case); i++) {
            _.merge(dataTable, {
                [i]: {
                id: i,
                confirmed_case: confirmed_case[i],
                confirmed_date: confirmed_date[i], 
                death_case: death_case[i],
                recovered_case: recovered_case[i],
                newConfirmed_case: newConfirmed_case[i],
                newDeath_case: newDeath_case[i],
                newRecovered_case: newRecovered_case[i]
                },
            });
        } 
        
        const toRenderDataTable = _.orderBy(_.toArray(dataTable),['confirmed_case'], ['desc']);
        console.log(toRenderDataTable);
        
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="COVID-19 Case"
                                category="Here is a subtitle for this table"
                                ctTableFullWidth
                                ctTableResponsive
                                content={
                                    <BootstrapTable
                                    data={toRenderDataTable}
                                    insertRow
                                    deleteRow
                                    exportCSV
                                    search
                                    hover
                                    pagination
                                    headerStyle={{
                                        background: '#325439',
                                        font: '#fcba03',
                                        color:'#fcba03'
                                    }}
                                >
                                    <TableHeaderColumn
                                        dataField="id"
                                        isKey
                                        dataAlign="center"
                                        hidden
                                        
                                    >
                                        ID
                                    </TableHeaderColumn>
                
                                    <TableHeaderColumn
                                        dataField="confirmed_date"
                                        width="175"
                                        dataAlign="center"
                                       
                                    >
                                        Date
                                    </TableHeaderColumn>
                                    <TableHeaderColumn
                                        dataField="confirmed_case"
                                        width="175"
                                    >
                                        Confirmed
                                    </TableHeaderColumn>
                                    <TableHeaderColumn
                                        dataField="death_case"
                                        width="175"
                                        dataSort
                                    >
                                        Deaths
                                    </TableHeaderColumn>
                                    <TableHeaderColumn dataField="recovered_case" width="175">
                                        Recovered
                                    </TableHeaderColumn>
                                    <TableHeaderColumn
                                        dataField="newConfirmed_case"
                                        width="175"
                                    >
                                        New Confirmed
                                    </TableHeaderColumn>
                                    <TableHeaderColumn
                                        dataField="newDeath_case"
                                        width="175"
                                    >
                                        New Deaths
                                    </TableHeaderColumn>
                                    <TableHeaderColumn
                                        dataField="newRecovered_case"
                                        width="175"
                                    >
                                        New Recovered
                                    </TableHeaderColumn>
                                </BootstrapTable>
                
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default TableList;
